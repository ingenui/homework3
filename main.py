import tkFileDialog
import matplotlib
import numpy as np
import cv2 as cv
import spectral_clustering as sp
import PIL.Image
from Tkinter import *
from tkFileDialog import askopenfilename
import test
import ghirlandina_template_match as gtm
import test
import canny
import os

class App:

    def __init__(self):
        self.job = None
        self.img_filename = None
        self.img=None
        self.img_morpho=None
        self.root = Tk()
        self.root.geometry("250x250+50+50")
        menu = Menu(self.root)
        self.root.config(menu=menu)
        filemenu = Menu(menu)
        menu.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(label="Open...", command=self.OpenFile)
        filemenu.add_command(label="Open solution", command=self.OpenSolutionFile)
        #filemenu.add_command(label="Test", command=self.BeginTest)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.root.quit)

        self.low_threshold_label = Label(self.root, text="THL").grid(row=0, column=0)        
        self.canny_low_threshold = Scale(self.root, from_=1, to=255, orient=HORIZONTAL, command=self.updateValue)
        self.canny_low_threshold.grid(row=0, column=1)        

        self.high_threshold_label = Label(self.root, text="THH").grid(row=1, column=0)      
        self.canny_high_threshold = Scale(self.root, from_=1, to=255, orient=HORIZONTAL, command=self.updateValue)
        self.canny_high_threshold.grid(row=1, column=1)        

        self.alpha_coefficient_label = Label(self.root, text="Alpha").grid(row=2, column=0)
        self.alpha_coefficient = Scale(self.root, from_=1, to=10, orient=HORIZONTAL, command=self.updateAlpha)
        self.alpha_coefficient.grid(row=2, column=1)

        self.method=IntVar()
        self.select_method_canny = Radiobutton(self.root, text="Canny", variable=self.method, value=1, command=self.update_slider_state).grid(row=3, column=0)
        self.select_method_kmeans = Radiobutton(self.root, text="Kmeans", variable=self.method, value=0, command=self.update_slider_state).grid(row=3, column=1)

        self.goButton=Button(self.root, text='Go', command=self.button_pressed).grid(row=4)
        self.canny_config_label = Label(self.root, text="Canny config:").grid(row=5, column=0)
        self.canny_config_mode_text = StringVar()        
        self.canny_config_mode_label = Label(self.root, textvariable=self.canny_config_mode_text).grid(row=5, column=1)

        self.solution_accuracy_label = Label(self.root, text="Solution accuracy:").grid(row=6, column=0)
        self.solution_accuracy_text = StringVar()        
        self.solution_accuracy_text_label = Label(self.root, textvariable=self.solution_accuracy_text ).grid(row=6, column=1)
        
        self.root.mainloop()

    def update_slider_state(self):
        if self.method.get()==0:
            self.canny_low_threshold.config(state=DISABLED, troughcolor='red')
            self.canny_high_threshold.config(state=DISABLED, troughcolor='red')
            #self.closure_coefficient.config(state=DISABLED, troughcolor='red')
            #self.opening_coefficient.config(state=DISABLED, troughcolor='red')
        else:
            self.canny_low_threshold.config(state=NORMAL, troughcolor='black')
            self.canny_high_threshold.config(state=NORMAL, troughcolor='black')
            #self.closure_coefficient.config(state=NORMAL, troughcolor='black')
            #self.opening_coefficient.config(state=NORMAL, troughcolor='black')

    def OpenFile(self):
        name = askopenfilename()
        self.img_filename = name
        self.color_img = cv.imread(self.img_filename, cv.IMREAD_COLOR)
        self.img = cv.imread(self.img_filename, cv.IMREAD_GRAYSCALE).astype(np.float)
        name_only = os.path.basename(self.img_filename)
        cv.imshow("Original image - " + name_only, self.img.astype(np.uint8))
        #histr = cv.calcHist([self.img.astype(np.uint8)], [0], None, [256], [0,256])
        #histr = histr / self.img.size
        #matplotlib.pyplot.plot(histr)
        #matplotlib.pyplot.xlim([0,256])
        #matplotlib.pyplot.show()


    def OpenSolutionFile(self):
        name = askopenfilename()
        self.solution_filename = name
        self.solution = np.asarray(PIL.Image.open(self.solution_filename))
        #cv.imshow("Solution", self.solution.astype(np.uint8)*127)


    


    def updateValue(self, event):
        if self.job:
            self.root.after_cancel(self.job)            
                        
        self.job = self.root.after(300, self.refresh_image)
        
    

    def label_to_color(self, img):
        # if self.img is None:
        #     return 0
        map={
            0: (0,0,0),
            1: (255, 0, 255),
            2: (0, 255, 0)
        }
        imgc = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
        imgc[img == 0] = map[0]
        imgc[img == 1] = map[1]
        imgc[img == 2] = map[2]
        return imgc

    def updateAlpha(self, value):
        if self.img is None or self.img_segmented is None:
            return 0
        alpha=np.array([self.alpha_coefficient.get()/10., self.alpha_coefficient.get()/10., self.alpha_coefficient.get()/10.])
        out_img = self.color_img * alpha
        out_img += self.label_to_color(self.img_segmented) * (1.-alpha)
        name_only = os.path.basename(self.img_filename)
        cv.imshow(self.metodo + " - " + name_only, (out_img).astype(np.uint8))

    def button_pressed(self):
        if self.img is None:
            return 0
        self.canny_lt, self.canny_ht, self.canny_mode = canny.findCannyConfiguration(self.img.astype(np.uint8))
        self.canny_low_threshold.set(self.canny_lt)
        self.canny_high_threshold.set(self.canny_ht)
        self.canny_config_mode_text.set(self.canny_mode)

        self.refresh_image()

    def refresh_image(self):
        self.job=None
        if self.img is None:
            return 0
        
        if(self.method.get()==0):
            self.img_segmented = sp.main_kmeans_clustering_image(self.img)
            self.metodo = 'K-means'
        else:
            self.img_segmented = canny.cannySeparate(self.img, self.canny_low_threshold.get(), self.canny_high_threshold.get())
            self.metodo = 'Canny'

        self.img_segmented = gtm.ghirlandina_match(self.img.astype(np.uint8), self.img_segmented.astype(np.uint8))
        self.updateAlpha(1)
        if not (self.solution is None):
            self.accuracy = float(np.sum(self.img_segmented==self.solution))/(self.img.shape[0]*self.img.shape[1])                        
            self.solution_accuracy_text.set(str(self.accuracy))

        # closure_kernel = np.ones((self.opening_coefficient.get(), self.opening_coefficient.get()), np.uint8)
        # uni = np.ones(int(math.ceil(self.img.shape[0]/2.)), dtype=np.float)
        # gradient = np.arange(0., 0.1, 0.1/(self.img.shape[0]/2.))
        # gradient = np.concatenate((gradient, uni), axis=0)
        # gradient = np.expand_dims(np.array(gradient), axis=1)
        # gradient = np.broadcast_to(gradient, (self.img.shape[0], self.img.shape[1]))
        # cv.imshow("Magnitude", opened)
        # edges_modified=edges*gradient
        # cv.imshow("Edges modified", (edges_modified).astype(np.uint8))


        # sobelx_5x5_kernel = np.array([[-1, -2, 0, 2, 1], [-2, -3, 0, 3, 2], [-3, -5, 0, 5, 3], [-2, -3, 0, 3, 2 ], [-1, -2, 0, 2, 1]])
        # sobely_5x5_kernel = -sobelx_5x5_kernel.transpose()
        # sobel_x_kernel = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], dtype=np.float)
        # sobel_y_kernel = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], dtype=np.float)
        # sobelx = cv.filter2D(self.img, -1, sobel_x_kernel)
        # sobely = cv.filter2D(self.img, -1, sobel_y_kernel)
        # magnitude = np.sqrt(np.power(sobelx, 2) + np.power(sobely, 2)) * 255 / np.sqrt((255 * 3) ** 2 * 2)
        # laplacian_kernel = np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]], dtype=np.float)
        # edges = cv.filter2D(self.img, -1, laplacian_kernel).astype(np.uint8)
        # kernel = np.ones((16, 16), np.uint8)
        # cv.imshow("Segmented", self.img)

    def BeginTest(self):
        test.test_video()

if __name__=='__main__':
    # gtm.new_template()
    app=App()
    #test.test_video()