import cv2 as cv
import matplotlib
import numpy as np

def findCannyConfiguration(gray_img):
        hist = cv.calcHist([gray_img.astype(np.uint8)], [0], None, [256], [0,256])
        norm_hist = hist / gray_img.size
        matplotlib.pyplot.plot(hist)
        matplotlib.pyplot.xlim([0,256])        

        acc = 0.0
        perc_20 = 0
        perc_80 = 0
        for pixel, freq in enumerate(norm_hist):
            acc += freq[0]           
            if (acc >= 0.2):
                perc_20 = pixel
                break
            
        acc = 0
        for pixel, freq in enumerate(norm_hist):
            acc += freq[0]           
            if (acc >= 0.8):
                perc_80 = pixel
                break

        global_hist_variance = hist.var()        
        val = (perc_80 - perc_20) / 2 + perc_20

        matplotlib.pyplot.axvline(np.uint8(val))
        matplotlib.pyplot.show()

        if val < 50:            
            return (5, 50, 'Notte')
        elif val > 100 and val < 200:            
            return (5, 130, 'Giorno')
        elif val >= 50 and val <= 100:            
            return (120, 200, 'Tramonto nuvoloso')
        else:            
            return (5, 70, 'Tramonto sereno')

def cannySeparate(img, lth, hth):
        edges = cv.Canny(img.astype(np.uint8), lth, hth)
        closure_kernel_size = 70        
        opening_kernel_size = 100
        closure_kernel = np.ones((closure_kernel_size, closure_kernel_size), np.uint8)
        closed = cv.morphologyEx(edges, cv.MORPH_CLOSE, closure_kernel)
        opening_kernel = np.ones((opening_kernel_size, opening_kernel_size), np.uint8)
        opened = cv.morphologyEx(closed, cv.MORPH_OPEN, opening_kernel)
        opened[opened==255]=1
        return opened