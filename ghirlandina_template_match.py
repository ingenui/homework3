import numpy as np
import cv2 as cv
import PIL
import fnmatch
import os

ghirlandina_color=(0, 255, 255)

def new_template():
    original_img=cv.imread('shot_keyframe\\video4_shot5\\video4_shot5.jpg')
    label = np.asarray(PIL.Image.open('shot_keyframe\\video4_shot5\\video4_shot5.png'))
    row_min=label.shape[1]+1
    row_max=-1
    col_min=label.shape[0]+1
    col_max=-1
    for (y,x), pixel in np.ndenumerate(label):
        if (pixel==2):
            if(y<row_min):
                row_min=y
            if(y>row_max):
                row_max=y
            if (x < col_min):
                col_min = x
            if (x > col_max):
                col_max = x
    ghirlandina_rect = label[row_min:row_max+1, col_min:col_max+1]
    template = cv.Canny(original_img[row_min:row_max+1, col_min:col_max+1], 5, 130)
    cv.imwrite('templates\\video4_shot5_ghirlandina_template.png', template)
    cv.imshow('template', template)
    cv.imwrite('video4_shot5_ghirlandina_label.png', ghirlandina_rect)
    cv.imshow('ghir', (ghirlandina_rect*120).astype(np.uint8))
    #cv.imshow("Label", (label*120).astype(np.uint8))
    cv.waitKey(0)
    cv.destroyAllWindows()
    return 0

def ghirlandina_match(img, out_img):
    templates_filename=[]
    templates=[]
    ghirlandina_labels_filename=[]
    ghirlandina_labels=[]
    module_current_dir = os.path.dirname(__file__)
    templates_dir = module_current_dir + "\\templates"
    for filename in os.listdir(templates_dir):
        if fnmatch.fnmatch(filename, '*template.png'):
            templates_filename.append(filename)
        if fnmatch.fnmatch(filename, '*label.png'):
            ghirlandina_labels_filename.append(filename)
    for template_filename in templates_filename:
        template = cv.imread(templates_dir + "\\" + template_filename, cv.IMREAD_GRAYSCALE)
        templates.append(template)
    for f in ghirlandina_labels_filename:
        g = cv.imread(templates_dir + "\\" + f, cv.IMREAD_GRAYSCALE)
        ghirlandina_labels.append(g)

    maxPoint=0
    maxIndex=0
    maxMaxLoc=(0,0)
    for i, t in enumerate(templates):
        canny_img=cv.Canny(img, 5, 130)
        resp = cv.matchTemplate(canny_img, t, cv.TM_CCORR_NORMED)
        minVal, maxVal, minLoc, maxLoc = cv.minMaxLoc(resp)
        if maxVal>maxPoint:
            maxPoint=maxVal
            maxIndex=i
            maxMaxLoc=maxLoc

    t_height = templates[maxIndex].shape[0]
    min_row = maxMaxLoc[1]
    max_row = min_row+t_height

    t_width = templates[maxIndex].shape[1]
    min_col = maxMaxLoc[0]
    max_col = min_col + t_width

    for y in range(min_row,max_row):
        for x in range(min_col,max_col):
            if ghirlandina_labels[maxIndex][y - min_row, x-min_col]==2:
                out_img[y,x]=2
    return out_img

        # cv.imshow('ghir', out_img*127)
        # cv.waitKey(0)
        # cv.destroyAllWindows()