***************
* CASI FELICI *
***************
+ Canny
	- Test (5).png (THL 5, THH 130, C 70, A 100)			SERENO
		Comprende montagne, orizzonte circa preciso
	- Test (8).png (THL 5, THH 130, C 70, A 100)			TRAMONTO NUVOLOSO
		Segue bene la città
	- video5_shot3.png (THL 8, THH 70, C 70, A 100) 		NOTTE
		Bene bene	
	- video3_shot1.png (THL 5, THH 70, C 70, A 100)			TRAMONTO SERENO
		Segue le montagne, sborda un po
	
+ K-means
	- Test (5).png
		Segue le montagne, abbastanza preciso
	- video2_shot3.png										TRAMONTO NUVOLOSO
		Segue bene orizzonte. Alcuni buchi
	- video3_shot1.png										TRAMONTO SERENO
		Segue bene orizzonte. Alcuni buchi piccoli

******************
* CASI INTERMEDI *
******************
+ Canny
	- Test (4).png (THL 8, THH 53, C 70, A 100) 			NOTTE
		Così, così. Buco a destra
		
*****************
* CASI INFELICI *
*****************
+ Canny
	- Test (2).png (THL 120, THH 200, C 70, A 100)			NUOVOLE
		Non buono, linea città bassa
	- Test (1).png (THL 8, THH 76, C 70, A 100)				TRAMONTO
		Decisamente male. Squadretta
	- video2_shot3.png (THL 100, THH 200, C 70, A 100)		NUVOLE
		Decisamente male. Squadretta. Parti città scoperte

+ K-means
	-Test (8).png											TRAMONTO NUVOLOSO
	-Test (2).png											TRAMONTO NUVOLOSO
		Linea orizzonte altissima
	-Test (4).png 											NOTTE
	-Test (1).png											TRAMONTO SERENO
	-video5_shot3.png										NOTTE
		Linea orizzonte alta, ma meglio canny		