import cv2
import numpy as np
import spectral_clustering as sp
import ghirlandina_template_match as gtm
import PIL
import canny

def label_to_color(img):
    map = {
        0: (0, 0, 0),
        1: (255, 0, 255),
        2: (0, 255, 0)
    }
    imgc = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    imgc[img == 0] = map[0]
    imgc[img == 1] = map[1]
    imgc[img == 2] = map[2]
    return imgc

def test_video(method, canny_lth = 0, canny_hth = 0):
    solution = np.asarray(PIL.Image.open('shot_keyframe\\video1_shot2\\video1_shot2_label.png'))
    cap = cv2.VideoCapture('Modena_1.mp4')
    while not cap.isOpened():
        cap = cv2.VideoCapture("Modena_1.mp4")
        cv2.waitKey(1000)
        print "Wait for the header"
    begin_frame = 250
    end_frame = 1100
    passo=10
    cap.set(1, begin_frame)
    accuracies=[]
    for current_frame_index in range(begin_frame, end_frame, passo):
        _, current_frame = cap.read()
        gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
        if method == 0:
            img_segmented = sp.main_kmeans_clustering_image(gray)
        else:
            img_segmented = canny.cannySeparate(gray, canny_lth, canny_hth)

        img_segmented = gtm.ghirlandina_match(gray.astype(np.uint8), img_segmented.astype(np.uint8))

        alpha = np.array([0.5, 0.5, 0.5])
        out_img = current_frame * alpha
        out_img += label_to_color(img_segmented) * (1. - alpha)
        print 'Frame ', current_frame_index/passo, ' di ', (end_frame-begin_frame)/passo

        accuracy = float(np.sum(img_segmented == solution)) / (gray.shape[0] * gray.shape[1])*100
        accuracies.append(accuracy)

        ghirlandina_precision = np.float((np.logical_and(img_segmented==2, solution==2).sum()))/(np.sum(img_segmented==2))
        ghirlandina_recall = np.float(np.logical_and(img_segmented==2, solution==2).sum())/(np.sum(solution==2))
        city_precision = np.float(np.logical_and(img_segmented==1, solution==1).sum())/(np.sum(img_segmented==1))
        city_recall = np.float(np.logical_and(img_segmented==1, solution==1).sum())/(np.sum(solution==1))
        print("Accuracy: {0:.2f}%".format(accuracy))
        print("Ghirlandina precision: {0:.2f}%".format(ghirlandina_precision))
        print("Ghirlandina recall: {0:.2f}%".format(ghirlandina_recall))
        print("City precision: {0:.2f}%".format(city_precision))
        print("City recall: {0:.2f}%".format(city_recall))

        cv2.imshow("Tut 2", (out_img).astype(np.uint8))
        cv2.waitKey(0)
    print "Accuracy media: {0:.2f}%".format(np.mean(accuracies))