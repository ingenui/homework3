import numpy as np
from sklearn.cluster import KMeans, SpectralClustering
import skimage.io
import skimage.transform
import cv2 as cv

import matplotlib.pyplot as plt
plt.ion()


# def similarity_function_image(a,b, sigma):
#     weights=np.array((0.2,1.0,0.0), dtype=np.float32)
#     num_value=np.sum(np.abs(a-b)*weights)
#     return np.exp(-num_value/sigma)

def main_kmeans_clustering_image(img):
    """
    Main function for spectral clustering.
    """
    num_cluster=2
    scaling_factor=2
    img=skimage.transform.rescale(img,1./scaling_factor,preserve_range=True)
    w,h=img.shape
    img[w-int(w/10):w, :]=0
    # Mettiamo tutti i pixel srotolati in un ndarray con features il colore, la coordinata x e y
    colors = np.zeros((w * h, 3))
    # Pesi delle varie features per il calcolo della distanza
    peso_c = 0.4
    peso_w = 0.5
    peso_h = 0.9
    count = 0
    for i in range(0, w):
        for j in range(0, h):
            colors[count, :] = np.hstack((img[i, j]/255 * peso_c, np.float(i)/w * peso_h, np.float(j)/h * peso_w))
            count += 1
    labels = KMeans(n_clusters=num_cluster).fit(colors).labels_
    if labels[-1]==0:
        labels+=1
        labels%=2
    # visualize results
    imglabels = np.reshape(labels, (w, h))
    imglabels = skimage.transform.rescale(imglabels,scaling_factor,preserve_range=True)
    imglabels[imglabels>0]=1
    return imglabels.astype(np.uint8)



